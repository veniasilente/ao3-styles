# ao3-styles

A few style templates and models I'm using for Work Styles in my AO3 profile. They are built on top of the site's default "Basic" style.

Styles here cover primarily three aspects:

 * Supplementing styles (eg.: cursive font) and typographical effects (indentation, small-caps, etc).
 * Prefering Libre / free resources (eg.: Liberation fonts before Microsoft's fonts).
 * Adding styles that emphasize a 1/3rd or 2/3rd column design (tables, floatboxes, etc).


## Installation

Create a new CSS file and ``cat`` or copy over the styles from the files you want.

Copy the resulting file into a named Work Skin.

Adjust as needed.

## Usage

Decorate the elements of the chapter's HTML with the styles as needed. The [official AO3 FAQ on Work Skins](https://archiveofourown.org/faq/tutorial-creating-a-work-skin?language_id=en) covers the overall procedure.

## Contents

* [ao3-sitestyle-basicimproved.css] — General layout and typography styles used by default in my stories.

Some impirtant class names that are defined:

* ``print``: Applied to a ``div``, will format its children ``p`` with spacing between paragraphs.
* ``flashback`` for flashback sections, has left indent and wavy left border.
* ``epistolary`` for IU documents, has side margin and straight side borders.
* ``REDACTED`` for spans of lines and sections that are redacted over.
* ``EVIL`` for spans of red-speak as it's used in WAAPT.
* ``newspaper`` for <hX> elements with style.
* ``newsreel`` for <hX> elements with style.
* ``font-{fallback}``: styles general font fallbacks (eg.: "sans-serif") preferring Libre fonts where available.
* ``metric-{font}``: styles text with Libre fonts metric-compatible to proprietary fonts.
* ``reflink``: styles links so they are distinguishable when they point to select targets such as Wikipedia.

## Extensions

Each file in ``styles/`` contains a selection of styles that can be added together to realize various effects. These effects include:

* ``color-x11.css``: gives class name to X11 / SVG colours. Not needed if colour is used sparingly. (background-colors is pending)
* ``dl-sidenote.css``: implements paragraph-level footnotes by styling definition lists (``<dl>``).
* ``ruby.css``: Ruby presentation fallback, obsolete since late 2023.
* ``terminal.css``: styles content as if it were computer terminal output.


## License

Creative Commons **BY-NC-SA 4.0**. See [LICENSE] for details.

